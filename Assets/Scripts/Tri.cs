﻿using UnityEngine;
using System.Collections;

public class Tri : MonoBehaviour {

	private int kills = 0;
	private Color orig_color;

	public void Start(){
		orig_color = this.gameObject.GetComponent<SpriteRenderer>().color;
	}
	
	public void Run(Color curr_player_color){
		kills += 1;
		this.gameObject.GetComponent<SpriteRenderer>().color = Color.Lerp (orig_color, curr_player_color, 1.0f);
	}
}
