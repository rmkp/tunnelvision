﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour {
	
	// references
	public Transform player_target;
	public GameObject level_gen;

	// misc
	Vector3 cam_pos;
	Vector3 init_pos;

	private bool moving = true;


	// shake stuff
	private float time_StartShake = 0f;
	private float time_ShakeDuration = 0.5f;
	private float r_mag = 0.2f; // maximum translation for shake
	private bool shaking = false;
	Vector3 rand = Vector3.zero;

	// reset stuff
	private bool reseting = false;
	private float time_StartReset = 0f;
	private float time_ResetDuration = 1.5f;
	Vector3 reset_pos;

	// jumping stuff
	private float jump_cam_move_mag = 2f;
	private float jump_cam_move = 0f;
	private float time_StartJump = 0f;

	private int level_len = 10;

	void Start () {
		transform.position = new Vector3 (player_target.position.x, player_target.position.y, transform.position.z);
		cam_pos = transform.position;
		init_pos = cam_pos;


	}

	void Update () {

		level_len = level_gen.GetComponent<LevelGen>().level_len;

		Vector3 p = cam_pos;
		float t = 0f;


		if (shaking){
			t = ((Time.timeSinceLevelLoad - time_StartShake)/(time_ShakeDuration));
		

			if (t >= 0 && t <= 1){
				rand = new Vector3(Random.Range (-r_mag, r_mag),Random.Range (-r_mag, r_mag),Random.Range (-r_mag, r_mag));
				t = 1 - t;
				rand *= t;
			} else if (t > 1){
				StartReset();
			}

			// set pos
			this.gameObject.transform.position = new Vector3(cam_pos.x + rand.x, cam_pos.y + rand.y, cam_pos.z + rand.z);
			this.gameObject.transform.LookAt(new Vector3(0f,cam_pos.y + rand.y, player_target.position.z));

		} else if (reseting){
			t = ((Time.timeSinceLevelLoad - time_StartReset)/(time_ResetDuration));
			if (t >= 0 && t <= 1){
				t *= t;
				Vector3 lerped_pos = Vector3.Lerp (reset_pos, init_pos, t);
				float extra_cam_dist = Mathf.Sin(t*Mathf.PI)*250f;
				this.gameObject.transform.position = new Vector3(lerped_pos.x, lerped_pos.y, lerped_pos.z - extra_cam_dist);	
			}
			if (t > 1){
				reseting = false;
				cam_pos = init_pos;
			}

		} else if (moving){
			cam_pos = new Vector3(p.x, p.y + Time.deltaTime*7f, p.z);


			float jump_t = ((Time.timeSinceLevelLoad - time_StartJump)/(0.6f));
			float final_jump_cam_move = 0f;
			/*
			if (jump_t > 0f && jump_t < 1f){
				final_jump_cam_move = Mathf.Sin(jump_t*Mathf.PI)*jump_cam_move;
			} */

			float final_y = cam_pos.y + rand.y;
			if (final_y > level_len){
				final_y = level_len;
			}
		


			this.gameObject.transform.position = new Vector3(cam_pos.x + rand.x + final_jump_cam_move, final_y, cam_pos.z + rand.z);
			//this.gameObject.transform.LookAt(new Vector3(cam_pos.x + rand.x,cam_pos.y + rand.y, player_target.position.z));




		} else {
			Debug.Log ("stationary cam");
		}


	}
	

	public void StartShake(){
		shaking = true;
		time_StartShake = Time.timeSinceLevelLoad;
	}

	private void StartReset(){
		reseting = true;
		shaking = false;
		time_StartReset = Time.timeSinceLevelLoad;
		reset_pos = this.gameObject.transform.position;
	}


	public void StartJump(float dir){
		time_StartJump = Time.timeSinceLevelLoad;
		jump_cam_move = jump_cam_move_mag * dir;
	}

	public void EndJump(){
		jump_cam_move = 0f;
		//this.gameObject.transform.LookAt(new Vector3(0f,cam_pos.y + rand.y, player_target.position.z));
	}

	public void StartMove(){
		moving = true;
	}

	public void StopMove(){
		moving = false;
	}





}
