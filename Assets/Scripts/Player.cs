﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	// references
	public GameObject cam;
	public GameObject level_gen;


	// level global params
	private float move_direction = 1f;
	private float move_velocity = 10f;
	private float level_speed = 7f;
	private int init_jumps = 0;

	// level params to be set
	public Color[] player_colors;
	private int num_colors = 10; // I'm planning on not using "player_colors", and will gen colors instead

	public AudioClip jump;
	public AudioClip land;
	public AudioClip death;
	public AudioClip powerup;
	public AudioClip start;

	private AudioSource audioJump;
	private AudioSource audioLand;
	private AudioSource audioDeath;
	private AudioSource audioPowerup;
	
	// interal params
	private Vector3 init_pos;
	private int jumps;
	private float x = 0f;
	public bool is_active;
	private bool moving = true;
	
	// etc
	private bool reseting = false;
	private float time_StartReset = 0f;
	private float time_ResetDuration = 1.5f;
	Vector3 reset_pos;

	// set up audio sources
	AudioSource AddAudio(AudioClip clip, bool loop, bool playAwake, float vol) { 
		AudioSource newAudio = gameObject.AddComponent<AudioSource>();
		newAudio.clip = clip; 
		newAudio.loop = loop;
		newAudio.playOnAwake = playAwake;
		newAudio.volume = vol; 
		return newAudio; 
	}

	void Awake(){
		audioJump = AddAudio(jump, false, false, 1.0f);
		audioLand = AddAudio(land, false, false, 0.5f);
		audioDeath = AddAudio(death, false, false, 1.0f);
		audioPowerup = AddAudio(powerup, false, false, 0.5f); 
	}
	
	void Start () {
		is_active = true;
		jumps = init_jumps;
		init_pos = transform.position;
		x = move_direction * move_velocity;
		reseting = false;
		player_colors = GenPlayerColors(num_colors);
		ChangeColor(jumps);
		level_gen.GetComponent<LevelGen>().ResetDiscs();
		level_gen.GetComponent<LevelGen>().RebuildBG(10);
		num_colors = level_gen.GetComponent<LevelGen>().num_colors;
	}
	
	void Update () {
		Vector3 p = this.gameObject.transform.position;

		if (jumps > 0 && is_active){
			if (Jumped()){
				move_direction *= -1f;
				cam.GetComponent<MainCamera>().StartJump (move_direction);
				x = move_direction * move_velocity;
				jumps -= 1;
				ChangeColor(jumps);
				audioJump.Play ();
			}
		}

		if (reseting){
			float t = ((Time.timeSinceLevelLoad - time_StartReset - 0.5f)/(time_ResetDuration));
			if (t >= 0 && t <= 1){
				t *= t;
				Vector3 lerped_pos = Vector3.Lerp (reset_pos, init_pos, t);
				this.gameObject.transform.position = new Vector3(lerped_pos.x, lerped_pos.y, lerped_pos.z);	
			} else if (t > 1){
				Start();
			}

		} else if (is_active && !reseting && moving) {
			float new_x = p.x + Time.deltaTime*x;
			float new_y = 0f;

			new_x = Mathf.Clamp(new_x, -3f, 3f);
			new_y = p.y + Time.deltaTime*level_speed;
			this.gameObject.transform.position = new Vector3(new_x, new_y, p.z);
		}

		if (p.y > level_gen.GetComponent<LevelGen>().level_len){
			level_gen.GetComponent<LevelGen>().RunLevelComplete();
		}



	}






	void OnTriggerEnter2D (Collider2D c){

		if (c.tag == "wall" && is_active){ // hit wall, you get another jump!

			cam.GetComponent<MainCamera>().EndJump();

			audioLand.Play ();
			x = 0f;
			jumps += 1;
			ChangeColor(jumps);
		}

		if (c.tag == "reset" && is_active){ // hit enemy... reset level
			StartReset(c.gameObject);
		}

		if (c.tag == "disc" && is_active){ // hit powerup, increment # jumps
			audioPowerup.Play ();
			jumps += 1;
			ChangeColor(jumps);
			c.gameObject.SetActive (false);
		}
	
	}

	private void StartReset(GameObject o){

		audioDeath.Play ();
		o.GetComponent<Tri>().Run (this.gameObject.GetComponent<SpriteRenderer>().color);
		cam.GetComponent<MainCamera>().StartShake ();
		is_active = false;

		reseting = true;
		time_StartReset = Time.timeSinceLevelLoad;
		reset_pos = this.gameObject.transform.position;
	}

	void ChangeColor(int jumps){
		int idx = Mathf.Clamp (jumps, 0, player_colors.Length);
		this.gameObject.GetComponent<SpriteRenderer>().color = player_colors[idx];
		this.transform.GetComponentInChildren<Text> ().text = idx.ToString ("0");
	}

	bool Jumped(){
		bool ret = false;
		
		/// touch input logic
		if (Input.touchCount > 0) {
			foreach (Touch touch in Input.touches) {
				switch (touch.phase) {
				case TouchPhase.Began:
					ret = true;
					break;
				case TouchPhase.Canceled:
					break;
				case TouchPhase.Ended:
					break;
				}
			}
		}
		
		/// keyboard in put logic
		if (Input.GetKeyDown (KeyCode.Space)) { ret = true; }
		if (Input.GetKeyUp (KeyCode.Space)) {}
		return ret;
	}


	Color[] GenPlayerColors(int _num_colors){
		Color[] ret = new Color[_num_colors];
		float first_hue = Random.Range (0f,360f);
		float first_saturation = 0.6f;
		for (int i = 0; i < _num_colors; i++){
			ret[i] = ColorUtil.ColorFromHSV(first_hue,first_saturation,1);
			first_hue += 43f;
			first_saturation *= 0.9f;
			first_hue = first_hue%360;
		}
		return ret;
	}


}
