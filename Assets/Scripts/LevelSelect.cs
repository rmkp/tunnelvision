﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class LevelSelect : MonoBehaviour {

	private float fadeout_duration = 1f;
	private float fadeout_start;
	private bool fadeout = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (GetInput()){
			fadeout = true;
			fadeout_start = Time.timeSinceLevelLoad;
		}


		if (fadeout){

			float t = 1 - ((Time.timeSinceLevelLoad - fadeout_start) / fadeout_duration);

			this.gameObject.GetComponent<AudioSource>().volume = t;

			if (t <= 0){
				fadeout = false;
				fadeout_start = 0f;
				Application.LoadLevel ("level1");
			}

		}



	}

	bool GetInput(){
		bool ret = false;
		
		/// touch input logic
		if (Input.touchCount > 0) {
			foreach (Touch touch in Input.touches) {
				switch (touch.phase) {
				case TouchPhase.Began:
					ret = true;
					break;
				case TouchPhase.Canceled:
					break;
				case TouchPhase.Ended:
					break;
				}
			}
		}
		
		/// keyboard in put logic
		if (Input.GetKeyDown (KeyCode.Space)) { ret = true; }
		if (Input.GetKeyUp (KeyCode.Space)) {}
		return ret;
	}



}
