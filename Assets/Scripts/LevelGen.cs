﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelGen : MonoBehaviour {

	// prefabs
	public GameObject tri;
	public GameObject tri2;
	public GameObject disc;
	public GameObject bg_obj;
	public GameObject finish_line_obj;
	public GameObject finish_line_txt;

	//references
	public GameObject bg;
	public GameObject wall_left;
	public GameObject wall_right;
	public GameObject bg_els_container;
	public GameObject player;

	//new objects/vars
	private List<GameObject> disc_clones = new List<GameObject> ();
	private List<GameObject> finish_line_objs = new List<GameObject> ();
	private List<GameObject> bg_obs = new List<GameObject> ();
	
	// colors
	public Color tri_color;
	public Color disc_color;


	public int num_colors = 10; // for player colors
	public int level_len = 1;
	private bool level_complete = false;

	private float song_fadeout_start = 0f;
	private bool fading_out = false;
	private float fade_out_duration = 3f;
	
	// Use this for initialization
	void Start () {
	
		Application.targetFrameRate = 120;
		QualitySettings.antiAliasing = 2;

		// read in level from file
		TextAsset l = Resources.Load ("1") as TextAsset;
		string[] lines = l.text.Split ("\n" [0]);
		level_len = lines.Length;
		Debug.Log ("level len: " + level_len);

		Vector3 orig_pos = bg.transform.position;
		bg.transform.position = new Vector3(orig_pos.x,level_len*0.5f,orig_pos.z);
		bg.transform.localScale = new Vector3(200f,level_len*5f, 1f);

		orig_pos = wall_left.transform.position;
		wall_left.transform.position = new Vector3(orig_pos.x,level_len*0.5f ,0f);
		wall_left.transform.localScale = new Vector3(10f,level_len+1, 1f);

		orig_pos = wall_right.transform.position;
		wall_right.transform.position = new Vector3(orig_pos.x,level_len*0.5f,0f);
		wall_right.transform.localScale = new Vector3(10f,level_len+1, 1f);

		// loop through textfile... build level
		int[] block_types = { 0,1,2 };
		for (int i = 0; i < lines.Length; i++) {
			for (int j = 0; j < lines[0].Length; j++) {

				char this_char = lines [i][j];
				float this_val = (float) char.GetNumericValue (lines [i] [j]);

				float x = 0f;
		

				float y = (level_len - i - 1);
		


				Quaternion tri_rot = Quaternion.identity;
				if (j == 0){
					x = -3f + 0.01f;
					tri_rot = Quaternion.Euler(0,0,90);
				} else if (j == 2){
					x = 3f - 0.01f;
					tri_rot = Quaternion.Euler(0,0,-90);
				} else if (j == 1){
					x = this_val - 3f; // change this to have range
				}



				if (this_char == 'v' || this_char == '>' || this_char == '<'){

					GameObject tri_clone = Instantiate (tri, new Vector2 (x, y), tri_rot) as GameObject;
					tri_clone.GetComponent<SpriteRenderer> ().color = tri_color;
					tri_clone.transform.parent = transform;
				}
				if (this_val > 0 && this_val < 10){
					GameObject disc_clone = Instantiate (disc, new Vector2 (x, y), tri_rot) as GameObject;
					disc_clone.transform.parent = transform;
					disc_clone.GetComponent<SpriteRenderer> ().color = disc_color;
					disc_clones.Add (disc_clone);
				}
			}
		}

		RebuildBG(10);


		// finish line
		Color[] finish_line_colors = new Color[7];
		float first_hue = Random.Range (0f,360f);
		float first_saturation = 0.6f;
		for (int i = 0; i < 7; i++){
			finish_line_colors[i] = ColorUtil.ColorFromHSV(first_hue,first_saturation,1);
			first_hue += 43f;
			first_saturation *= 0.9f;
			first_hue = first_hue%360;
		}
		for (int i = -3; i < 4; i++){
			GameObject finish_line_clone = Instantiate (finish_line_obj, new Vector3 (i, level_len, 0f), Quaternion.identity) as GameObject;
			finish_line_clone.transform.GetComponent<SpriteRenderer>().color = finish_line_colors[i+3];
			finish_line_clone.transform.parent = this.transform;
			finish_line_objs.Add (finish_line_clone);
		}
		finish_line_txt.transform.position = new Vector3(0f, level_len,0f);







	}

	void Update (){

		if (fading_out){
			float t = (Time.timeSinceLevelLoad - song_fadeout_start)/fade_out_duration;
			t = Mathf.Clamp (t,0f,1f);
			t = 1 - t;
			this.gameObject.GetComponent<AudioSource>().volume = t;

			float t2 = (Time.timeSinceLevelLoad - song_fadeout_start)/1f;
			t2 = Mathf.Clamp (t2,0f,1f);
			t2 = 1 - t2;
			Color fading_c = player.GetComponent<SpriteRenderer>().color;
			player.GetComponent<SpriteRenderer>().color = new Color(fading_c.r, fading_c.g, fading_c.b, t2);

			if (t <= 0){
				fading_out = false;
				Application.LoadLevel ("level_select");
			}


		}

	}

	public void ResetDiscs(){
		for (int i = 0; i < disc_clones.Count; i++) {
			disc_clones[i].SetActive (true);
			disc_clones[i].GetComponent<Disc>().Init ();
		}
	}

	public void RunLevelComplete(){
		if (!level_complete){
			level_complete = true;
			FinishExplode();
			finish_line_txt.GetComponent<Rigidbody>().velocity = new Vector3(0,1f,5f);

			this.gameObject.GetComponent<AudioSource>().volume = 0.1f;
			song_fadeout_start = Time.timeSinceLevelLoad;
			fading_out = true;

		}
	}

	public void FinishExplode(){
		for (int i = 0; i < finish_line_objs.Count; i++){
			Vector3 this_p = finish_line_objs[i].transform.position;
			finish_line_objs[i].GetComponent<Rigidbody2D>().velocity = new Vector2(this_p.x, Mathf.Cos (this_p.x)+3f);
		}
	}

	public void RebuildBG(int z_layers){

		foreach (Object o in bg_obs){
			Destroy(o);
		}
		bg_obs.Clear ();
		Color bg_el_c_top_front = ColorUtil.ColorFromHSV(Random.Range (0f,90f),0.8f,1f);
		Color bg_el_c_bottom_front = ColorUtil.ColorFromHSV(Random.Range (90f,180f),0.8f,1f);

		Color bg_el_c_top_back = ColorUtil.ColorFromHSV(Random.Range (180f,270f),0.8f,1f);
		Color bg_el_c_bottom_back = ColorUtil.ColorFromHSV(Random.Range (270f,360f),0.8f,1f);

		for (int x = -3; x <= 3; x+=2){
			for (int y = 0; y < level_len; y+=2){
				for (int z = 0; z < z_layers; z++){
					GameObject bg_obj_clone = Instantiate (bg_obj, new Vector3 (x, y, z*5f + 10f), Quaternion.identity) as GameObject;
					bg_obj_clone.transform.GetComponent<Rigidbody2D>().angularVelocity = z + 4;
					//float opacity = (1 - (z*1f/z_layers*1f)*0.8f) * 0.4f; // from 0.6 -> 0.1
					float opacity = Mathf.Lerp(0.6f, 0.0f, z*1f/z_layers*1f);

					Color this_color_front  = Color.Lerp(bg_el_c_top_front, bg_el_c_bottom_front, (y*1f) / level_len);
					Color this_color_back  = Color.Lerp(bg_el_c_top_back, bg_el_c_bottom_back, (y*1f) / level_len);
					Color this_color  = Color.Lerp(this_color_front, this_color_back, (z*1f) / z_layers);

					bg_obj_clone.transform.GetComponent<SpriteRenderer>().color = new Color(this_color.r, this_color.g, this_color.b, opacity);
					bg_obj_clone.transform.parent = bg_els_container.transform;
					bg_obs.Add (bg_obj_clone);
				}
			}
		}
	}







}
